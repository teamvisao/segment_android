### Projeto de Visão Computacional - Segmentação com Android

#### Segmentação no Android (100 PIC$)

#### Criar app para android que permite a escolha e aplicação em tempo real de duas técnicas de suavização, duas de segmentação e duas de detecção de bordas em vídeos capturados pela câmera do smartphone. O usuário deve ser capaz de alterar parâmetros dos algoritmos escolhidos. Perdendo 30PIC$, ao invés de uma APP pode ser feito um software para laptop/desktop (Ubuntu) usando a webcam no lugar da câmera do celular;


##Detalhes sobre as atividades envolvendo geração de código (programação de software):
 
- O aplicativo foi desenvolvido na IDE-Android Studio 3.1.3
-   Build #AI-173.4819257, built on June 4, 2018
-   JRE: 1.8.0_152-release-1024-b01 amd64
-   JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
-   Linux 4.13.0-45-generic


Para o conhecimento em visão computacional foi imprescindível, assistir os video relacionados do Professor Hemerson, entretando foram visualizados outros videos do uso da biblioteca opencv com Android Studio, e livros.
https://www.youtube.com/watch?v=0x9sw5YAero
https://www.youtube.com/watch?v=tdVQyEWH77A


- As etapas foram inicializadas procurando realizar uma revisão no conceitos básicos de desenvolvimento de aplicativos para o sistema operacional Android. Saliento realizei aulas e trocas de informações sobre android com o Aluno da UFMS Mario de Araújo Carvalho, que já tem grande experiência e que o mesmo indicou os curso da www.udemy.com, hoje tenho os cursos: 
Formação Cientista de Dados;
Curso de Desenvolvimento Android Oreo;
Básico de desenvolvimento de jogos;
Machine Learning e Data Science com Weka e Java;

- Após isso realizei pesquisa sobre como integrar o Android Studio com a bilbioteca (módulo) opencv.  - Ocorreram muitos problemas, pois, não foi encontrado materiais que pudesse sustentar meu conhecimento para realizar o mencionado desenvolvimento.


- O tempo de dedicação foram de 3,5 meses que junto com a disciplina visão computacional totalizava umas 10 horas semanais.
- Também criado video: https://www.youtube.com/watch?v=vU-S6zXVPLw&t=543s


**INSTALAÇÃO** 
- A IDE gera um apk, arquivo de instalação para o sistema operacional android, encontrado no diretório
```
    ├── outputs
│   │   └── apk
│   │       └── debug
│   │           └── app-debug.apk
```

E também na pasta raiz para facilitar o acesso.

- O código será testado utilizando sistema operacional Linux (distribuição Ubuntu 14.04 ou superior) e portanto deve estar preparado para ser compilado e executado nele (Ubuntu é um sistema operacional gratuito disponível na Internet e que pode ser instalado juntamente com o Windows em uma outra partição do disco rígido, usando uma máquina virtual, como a VirtualBox por exemplo, ou ainda a partir de um pendrive);



- A compilação e execução NÃO devem depender de plataformas específicas (E.g.: Netbeans, Eclipse, etc) e deve ser possível compilar e executar o código utilizando ferramentas padrões do Linux que funcionem por linhas de comando;
Em relação a execução os aplicativos android não permitem serem executados fora da plataforma, todavia, segue arquivo app-debug.apk para execução em dispositivos com sistema operacional android .


###Estrutura de diretórios
#Segue lista do principais arquivos em seus respectivos diretórios :
```
 src/
├── main
│   ├── AndroidManifest.xml (Configurar componentes do aplicativo Activities, Services, Content, Prodivers, permissões)
│   ├── java 
│   │   └── br
│   │       └── ucdb
│   │           └── menuinicial (Arquivos com código java - Manipulação do componentes xml - res)
│   │               ├── ColorBlobDetectionActivity.java (implements OnTouchListener, CvCameraViewListener2)
│   │               ├── ColorBlobDetector.java   (verifica o espaço de cores)
│   │               ├── CustomExpandableListAdapter.java  (Cria lista expansível personalizado e organiza)
│   │               ├── ExpandableListDataPump.java ( Classificação do itens da lista)
│   │               ├── MainActivity.java (Cria a lista expandida - ouvinte do click)
│   │               ├── MyCameraView.java ( implements PictureCallback )
│   │               └── VisaoActivity.java ( implements CvCameraViewListener2 )
│   ├── cpp (O grupo cpp é onde você pode encontrar todos os arquivos de origem, cabeçalhos e bibliotecas pré-compiladas que fazem parte do projeto)
│   │   ├── VisaoC.cpp  (código c++ - Corpo)
│   │   └── VisaoC.h    (código c++ - Cabeçalho/assinatura)
│   ├── outputs
│   │   └── apk
│   │       └── debug
│   │           └── app-debug.apk
│   ├── jniLibs (Biblioteca opencv compiladas, para dar suporte ao processamento de imagens)
│   │   └── arm64-v8a
│   │       ├── libopencv_calib3d.a
│   │       ├── libopencv_core.a
│   │       ├── libopencv_features2d.a
│   │       ├── libopencv_flann.a
│   │       ├── libopencv_highgui.a
│   │       ├── libopencv_imgcodecs.a
│   │       ├── libopencv_imgproc.a
│   │       ├── libopencv_java3.so
│   │       ├── libopencv_ml.a
│   │       ├── libopencv_objdetect.a
│   │       ├── libopencv_photo.a
│   │       ├── libopencv_shape.a
│   │       ├── libopencv_stitching.a
│   │       ├── libopencv_superres.a
│   │       ├── libopencv_video.a
│   │       ├── libopencv_videoio.a
│   │       └── libopencv_videostab.a
│   └── res (Arquivos de configuração e construção da interface)
│       ├── layout
│       │   ├── activity_main.xml
│       │   ├── activity_visao.xml
│       │   ├── color_blob_detection_surface_view.xml
│       │   ├── list_group.xml
│       │   └── list_item.xml
│       └── values (configurações de estilos, textos, internacionalizações, temas e dimensões)
│           ├── colors.xml
│           ├── strings.xml
│           └── styles.xml
├── build
│   └── outputs (Executável gerado para ser executado em um dispositivo móvel ou distribuido nas lojas de softwares móveis para Android)
│       └── apk
│           └── debug
│               └── app-debug.apk
├── build.gradle (Configurações sobre a construção do aplicativo, bibliotecas usadas no projeto, versão do aplicativo, nomes do package e configurações sobre o Sdk usado)
└── CMakeLists.txt (Configuração da biblioteca opencv e caminho do sdk) - ferramenta de compilação externa que funciona com o Gradle para compilar uma biblioteca nativa.

```
###------------------------------------------------------
### Permissão de acesso a câmera
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/00-permissaoCamera.png)
Ao instalar um app do Google Play em um dispositivo com Android 6.0 (ou superior) ou em um Chromebook, você controla quais funcionalidades ou informações o app pode acessar, o que é chamado de permissões. Por exemplo, um app pode precisar de permissão para ver os contatos ou o local do seu dispositivo. Você pode controlar quais permissões um app pode acessar após instalá-lo no dispositivo.

###------------------------------------------------------
### Tela Principal do aplicativo - Lista expansível personalizado e organiza
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/01-telaPrincipal.png)


### Tela Suavização
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/02-suavizacao.png)


### 01- Filtro da Média
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/03-blur.png)

### 02- Filtro Mediano
Antes
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/04-sal_pimenta.png)
Depois
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/044-sal_pimenta.png)


###------------------------------------------------------
### Tela Segmentação
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/05-segmentacao.png)

### 01- Segmentação - Binarização
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/06-05-segmentacao_bina.png)
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/05-segmentacao_bina_inv.png)

### 02- Segmentação - Cor
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/06-segmentacao_color.png)


###------------------------------------------------------
### 01- Detector de bordas - Canny
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/07-deteccao_Bordas.png)
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/07-canny.png)

### 02- Detector de Bordas - Sobel
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/07-sobel.png)







###--------------------------------------------------------------------------------------------------------------------
### Tela - Plus
Alguns experimentos que foram realizados para aprimorar meus conhecimento, que seguem.

![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/08-plus.png)


### Tela - Find Features
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/8919b1fb7d75a1e10abdcc81d26358c6b4d5ae95/img/09-plus.png)


### Tela - Cartoon
Alguns experimentos que foram realizados 
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/09-cartoon.png)


### Tela - Pixelize
Alguns experimentos que foram realizados 
![alt tag](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/09-pixel.png)


###--------------------------------------------------------------------------------------------------------------------
### Videos 
Foram realizados amostra com os videos demonstrando o aplicativo em operação:

##Média
[![Watch the video](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/03-blur.png)](https://youtu.be/2n8tfge-7gk)  

##Mediana
[![Watch the video](https://bitbucket.org/teamvisao/segment_android/raw/919c36b5f5033018dd652f41cda11296ccdf8d20/img/04-sal_pimenta.png)](https://youtu.be/01TG8dkD8u0)

##Binarização
[![Watch the video](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/06-05-segmentacao_bina.png))](https://www.youtube.com/watch?v=PJQdqwwsBwY)  


##Detector de cores
[![Watch the video](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/06-segmentacao_color.png)](https://youtu.be/us18A3pdFrY)


##Sobel e Canny
[![Watch the video](https://bitbucket.org/teamvisao/segment_android/raw/7dc950ed7fa2b0a186ff97383332b1106f5ad95c/img/07-deteccao_Bordas.png)](https://youtu.be/O0yl3IEvTXE)
###------------------------------------------------------

###Contato
e-mail: celso.costa@ifms.edu.br
Grupo de pesquisa: Inovisão - https://pistori.weebly.com/



