package br.ucdb.menuinicial;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.List;

public class VisaoActivity extends AppCompatActivity implements CvCameraViewListener2{
    private static final String    TAG      = "Trabalho::VisaoActivity";
    final private int SUAVIZA_BLUR  	    = 1;
    final private int SUAVIZA_MEDIAN    	= 2;

    final private int SEGMEN_BINARIZACAO    = 3;

    final private int BORDAS_SOBEL		    = 5;
    final private int BORDAS_CANNY	    	= 6;

    final private int PLUS_FEATURES	    	= 7;
    final private int PLUS_CARTOON		        = 9;
    final private int PLUS_PIXELIZE	        = 10;
    private Size      mSize0;


    private int TYPE  = 0;

    SeekBar mSeekBar01;
    SeekBar mSeekBar02;
    TextView mTextView01;
    TextView mTextView02;

    int mThreshold01 = 0;
    int mThreshold02 = 0;

    private MyCameraView mOpenCvCameraView;

    private int                     mViewMode;
    private Mat                     mRgba;
    private Mat                     mIntermediateMat;
    private Mat                     mGray;
    private Mat                     mSepiaKernel;

    private BaseLoaderCallback mBaseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status){
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    System.loadLibrary("find_features");
                }
                break;
                default:
                {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visao);
        Log.i(TAG, "called onCreate");

        mOpenCvCameraView = findViewById(R.id.java_Camera_View_CV);
        mOpenCvCameraView.setCvCameraViewListener(this);

        mSeekBar01  = findViewById(R.id.seekBarThreshold1);
        mSeekBar02  = findViewById(R.id.seekBarThreshold2);
        mTextView01 = findViewById(R.id.textT01);
        mTextView02 = findViewById(R.id.textT02);

        mSeekBar01.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mTextView01.setText(String.valueOf(i));
                mThreshold01 = i;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mSeekBar02.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mTextView02.setText(String.valueOf(i));
                mThreshold02 = i;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        Bundle dados = getIntent().getExtras();

        int cod_grupo =   dados.getInt("grupo");
        int cod_item  =   dados.getInt("item");

        Toast.makeText(
                getApplicationContext(),
                TYPE+" <= group " + cod_grupo + " item "
                        + cod_item, Toast.LENGTH_LONG);
        switch (cod_grupo){
            case 0:

                mSeekBar01.setVisibility(View.GONE);
                mTextView01.setVisibility(View.GONE);
                mSeekBar02.setMax(15);

                if (cod_item == 0){
                    TYPE = SUAVIZA_BLUR;
                }else{
                    TYPE = SUAVIZA_MEDIAN;
                }
                break;

            case 1:
                if (cod_item == 0){
                    TYPE = SEGMEN_BINARIZACAO;
                    //mSeekBar01.setVisibility(View.GONE);
                    //mTextView01.setVisibility(View.GONE);
                    mSeekBar01.setMax(255);
                    mSeekBar01.setProgress(255);
                    mSeekBar02.setMax(1);
                }else{
                    //Watershed
                    //TYPE = SEGMEN_COLORBLOB;
                }

                break;

            case 2:
                mSeekBar01.setMax(250);
                mSeekBar02.setMax(250);
                if (cod_item == 0){
                    //Sobel
                    TYPE = BORDAS_SOBEL;
                }else{
                    //Canny
                    TYPE = BORDAS_CANNY;
                }
                break;

            case 3:
                if (cod_item==0){
                    TYPE = PLUS_FEATURES;
                }else if(cod_item == 1){
                    TYPE = PLUS_CARTOON;
                    mSeekBar01.setVisibility(View.GONE);
                    mSeekBar02.setVisibility(View.GONE);
                    mTextView01.setVisibility(View.GONE);
                    mTextView02.setVisibility(View.GONE);
                } else {
                    TYPE = PLUS_PIXELIZE;
                    mSeekBar01.setVisibility(View.GONE);
                    mSeekBar02.setVisibility(View.GONE);
                    mTextView01.setVisibility(View.GONE);
                    mTextView02.setVisibility(View.GONE);
                }
                break;
        }
    }

    public VisaoActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }
    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mGray = new Mat(height, width, CvType.CV_8UC1);
        mIntermediateMat = new Mat(height, width, CvType.CV_8UC4);
        mSize0 = new Size();

        // Fill sepia kernel
        mSepiaKernel = new Mat(4, 4, CvType.CV_32F);
        mSepiaKernel.put(0, 0, /* R */0.189f, 0.769f, 0.393f, 0f);
        mSepiaKernel.put(1, 0, /* G */0.168f, 0.686f, 0.349f, 0f);
        mSepiaKernel.put(2, 0, /* B */0.131f, 0.534f, 0.272f, 0f);
        mSepiaKernel.put(3, 0, /* A */0.000f, 0.000f, 0.000f, 1f);

    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
        mGray.release();
        mIntermediateMat.release();
    }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);
        return new Scalar(pointMatRgba.get(0, 0));
    }


    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();
        Mat rgba = inputFrame.rgba();
        Size sizeRgba = mRgba.size();

        Mat rgbaInnerWindow;
        int rows = (int) sizeRgba.height;
        int cols = (int) sizeRgba.width;

        int left = cols / 8;
        int top = rows / 8;

        int width = cols * 3 / 4;
        int height = rows * 3 / 4;

        switch (TYPE){
            case SUAVIZA_BLUR :{
                if(mThreshold02  == 0){
                    mThreshold02 = 1;
                } else if((mThreshold02 % 2) == 0){
                    mThreshold02 = mThreshold02 - 1;
                }
                VisaoC(mGray.getNativeObjAddr(), mRgba.getNativeObjAddr(), 4, mThreshold01,mThreshold02);
            }
            break;

            case SUAVIZA_MEDIAN :{
                if(mThreshold02  == 0){
                    mThreshold02 = 1;
                } else if((mThreshold02 % 2) == 0){
                    mThreshold02 = mThreshold02 - 1;
                }
                VisaoC(mGray.getNativeObjAddr(), mRgba.getNativeObjAddr(), 3, mThreshold01,mThreshold02);
            }
            break;

            case SEGMEN_BINARIZACAO:{
                mRgba = inputFrame.rgba();
                int maxValue = 255;
                int blockSize = 61;
                int meanOffset = 15;
                Imgproc.adaptiveThreshold(
                        inputFrame.gray(),
                        mIntermediateMat,
                        mThreshold01, // maxValue
                        Imgproc.ADAPTIVE_THRESH_MEAN_C,
                        mThreshold02, // Imgproc.THRESH_BINARY_INV,
                        blockSize,
                        meanOffset
                );
                Imgproc.cvtColor(
                        mIntermediateMat,
                        mRgba,
                        Imgproc.COLOR_GRAY2RGBA,
                        4
                );
            }
            break;

            case BORDAS_SOBEL :{
                Imgproc.Sobel(inputFrame.gray(), mIntermediateMat, CvType.CV_8U, 1, 1);
                Core.convertScaleAbs(mIntermediateMat, mIntermediateMat, mThreshold01, mThreshold02);
                Imgproc.cvtColor(mIntermediateMat, mRgba, Imgproc.COLOR_GRAY2BGRA, 4);
            }
            break;

            case BORDAS_CANNY :{
                Imgproc.Canny(inputFrame.gray(), mRgba, mThreshold01, mThreshold02);
            }
            break;

            case PLUS_FEATURES:{
                mRgba = inputFrame.rgba();
                mGray = inputFrame.gray();
                VisaoC(mGray.getNativeObjAddr(), mRgba.getNativeObjAddr(), 1, mThreshold01,mThreshold02);
            }
            break;

            case PLUS_CARTOON:{
                mRgba = inputFrame.rgba();
                mGray = inputFrame.gray();
                VisaoC(mGray.getNativeObjAddr(), mRgba.getNativeObjAddr(), 2, mThreshold01, mThreshold02);
            }
            break;
            case PLUS_PIXELIZE:{
                rgbaInnerWindow = rgba.submat(top, top + height, left, left + width);
                Imgproc.resize(rgbaInnerWindow, mIntermediateMat, new Size(mThreshold01, mThreshold02), 0.1, 0.1, Imgproc.INTER_NEAREST);
                Imgproc.resize(mIntermediateMat, rgbaInnerWindow, rgbaInnerWindow.size(), 0., 0., Imgproc.INTER_NEAREST);
                rgbaInnerWindow.release();
            }
            break;
        }
        return mRgba;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[] {
                            Manifest.permission.CAMERA
                    }, 1);
        } else {
            if (!OpenCVLoader.initDebug()) {
                Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
                OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mBaseLoaderCallback);
            } else {
                Log.d(TAG, "OpenCV library found inside package. Using it!");
                mBaseLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Você deu permissão ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }
    public native void VisaoC(long matAddrGr, long matAddrRgba, int option, int parPri, int parSeg);

}
