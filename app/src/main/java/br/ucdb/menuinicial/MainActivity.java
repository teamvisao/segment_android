package br.ucdb.menuinicial;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String  TAG              = "MainActivity::TesteAcerto";

    ExpandableListView              expandableListView;
    ExpandableListAdapter           expandableListAdapter;
    List<String>                    expandableListTitle;
    LinkedHashMap<String, List<String>> expandableListDetail;

    // celso

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        expandableListView          = findViewById(R.id.expandableListView);
        expandableListDetail        = ExpandableListDataPump.getData();

        expandableListTitle         = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter       = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);

        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
//
//                Toast.makeText(
//                        getApplicationContext(),
//                        expandableListTitle.get(groupPosition)
//                                + (groupPosition == 3 && childPosition == 0 )+" -> " + groupPosition + " group "
//                                + " -> " + childPosition + " item "
//                                + expandableListDetail.get(
//                                expandableListTitle.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT
//                ).show();

                if (groupPosition == 1 && childPosition == 1  ){
                    Intent intentBlob = new Intent(MainActivity.this, ColorBlobDetectionActivity.class);
                    startActivity(intentBlob);

                }else {
                    Intent intent = new Intent(MainActivity.this, VisaoActivity.class);
                    intent.putExtra("grupo", groupPosition);
                    intent.putExtra("item", childPosition);
                    startActivity(intent);
                }
                return false;
            }
        });
    }
}
