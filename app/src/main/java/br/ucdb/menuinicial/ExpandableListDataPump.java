package br.ucdb.menuinicial;

/**
 * Created by costa on 22/03/18.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ExpandableListDataPump {
    public static LinkedHashMap<String, List<String>> getData() {
        //Cria a lista expandidada
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<>();
        List<String> suavizacao = new ArrayList<String>();
            suavizacao.add("Blur");
            suavizacao.add("Median Blur");

        List<String> segmentacao = new ArrayList<String>();
            segmentacao.add("Binarização");
            segmentacao.add("Detector de Cores");

        List<String> bordas = new ArrayList<String>();
            bordas.add("Sobel");
            bordas.add("Canny");

        List<String> plus = new ArrayList<String>();
            plus.add("Find Features");
            plus.add("Cartoon");
            plus.add("Pixelize");

        expandableListDetail.put("SUAVIZAÇÃO", suavizacao);
        expandableListDetail.put("SEGMENTAÇÃO", segmentacao);
        expandableListDetail.put("DETECÇÃO DE BORDAS", bordas);
        expandableListDetail.put("PLUS", plus);

        return expandableListDetail;
    }
}