#include <jni.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <vector>
#include <android/log.h>

#define LOG_TAG "menuinicial/VisaoC"
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

using namespace std;
using namespace cv;

extern "C" {
JNIEXPORT void JNICALL Java_br_ucdb_menuinicial_VisaoActivity_VisaoC(JNIEnv*, jobject, jlong addrGray, jlong addrRgba, jint option, jint parPri, jint parSeg) {
    Mat& mGr  = *(Mat*)addrGray;
    Mat& mRgb = *(Mat*)addrRgba;
    const int MEDIAN_BLUR_FILTER_SIZE = 7;
    vector<KeyPoint> v;
    int count = 0;

    if(option == 1) {
        Ptr<FeatureDetector> detector = FastFeatureDetector::create(50);
        detector->detect(mGr, v);
        for (unsigned int i = 0; i < v.size(); i++) {
            const KeyPoint &kp = v[i];
            //kp.size;
            circle(mRgb, Point(kp.pt.x, kp.pt.y), 20, Scalar(255, 0, 0, 255));
            //circle(mRgb, Point(55, 88), 10, Scalar(0,0,255,255));
            count++;
        }

        string opcaoTexto;  // string which will contain the resultPointers
        ostringstream convertOption;  // stream used for the conversion
        opcaoTexto     = convertOption.str(); // set 'resultPointers' to the contents of the stream
        convertOption << option;

        string resultPointers;  // string which will contain the resultPointers

        ostringstream convert;  // stream used for the conversion

        convert
                << count;       // insert the textual representation of 'Number' in the characters in the stream


        resultPointers = convert.str(); // set 'resultPointers' to the contents of the stream

        String text = opcaoTexto + " Pontos: " + resultPointers;
        int fontFace = FONT_HERSHEY_SIMPLEX;
        double fontScale = 2;
        int thickness = 3;

        Mat img = mRgb;

        int baseline = 0;
        Size textSize = getTextSize(text, fontFace,
                                    fontScale, thickness, &baseline);
        baseline += thickness;

        // center the text
        Point textOrg((img.cols - textSize.width) / 2,
                      (img.rows + textSize.height) / 2);

        // draw the box
        rectangle(img, textOrg + Point(0, baseline),
                  textOrg + Point(textSize.width, -textSize.height),
                  Scalar(0, 0, 255));
        // ... and the baseline first
        line(img, textOrg + Point(0, thickness),
             textOrg + Point(textSize.width, thickness),
             Scalar(0, 0, 255));

        // then put the text itself
        putText(img, text, textOrg, fontFace, fontScale,
                Scalar::all(150), thickness, 8);
    }else if(option == 2){ // PLUS_CARTOON

        const int MEDIAN_BLUR_FILTER_SIZE = 7;
        const int LAPLACIAN_FILTER_SIZE = 5;
        const int EDGES_THRESHOLD = 30;
        int repetitions = 5;
        int kSize = 9;
        double sigmaColor = 9;
        double sigmaSpace = 7;

        cv::Mat& edges = *(cv::Mat *) addrGray;
        cv::medianBlur(edges, edges, MEDIAN_BLUR_FILTER_SIZE);
        cv::Laplacian(edges, edges, CV_8U, LAPLACIAN_FILTER_SIZE);
        cv::Mat mask; cv::threshold(edges, mask, EDGES_THRESHOLD, 255, CV_THRESH_BINARY_INV);

        cv::Mat& src = *(cv::Mat *) addrRgba;
        cv::cvtColor(src,src,CV_RGBA2RGB);
        cv::Size size = src.size();
        cv::Size smallSize;
        smallSize.width = size.width/4;
        smallSize.height = size.height/4;
        cv::Mat smallImg = cv::Mat(smallSize, CV_8UC3);
        resize(src, smallImg, smallSize, 0, 0, CV_INTER_LINEAR);

        cv::Mat tmp = cv::Mat(smallSize, CV_8UC3);

        for(int i=0; i<repetitions;i++){
            bilateralFilter(smallImg, tmp, kSize, sigmaColor, sigmaSpace);
            bilateralFilter(tmp, smallImg, kSize, sigmaColor, sigmaSpace);
        }
        cv::Mat bigImg;
        resize(smallImg, bigImg, size, 0, 0, CV_INTER_LINEAR);
        cv::Mat dst; bigImg.copyTo(dst,mask);
        cv::medianBlur(dst, src, MEDIAN_BLUR_FILTER_SIZE-4);
    } else if(option == 3){ //Filtro de Madiano
        cv::medianBlur(mRgb, mRgb, parSeg);
    } else if(option == 4){ // Filtro de média
        cv::blur( mRgb, mRgb,  Size( parSeg, parSeg) );
    }

}
}